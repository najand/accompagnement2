#include <iostream>
#include <string>
#include <sstream>

using namespace std;

void exo1()
{

}

void exo2()
{

}

void exo3()
{

}

void exo4()
{

}

void exo5()
{

}

void exo6()
{

}

void exo7()
{

}

void exo8()
{

}

int main(int argc, char *argv[])
{
    if(argc > 1)
    {
        istringstream ss(argv[1]);
        int question = 0;
        ss >> question;
        
        if(question > 0 && question <= 8)
        {
            cout << "Exercice " << question  << " : " << endl;
            
            switch(question)
            {
                case 1:
                    exo1();
                    break;
                case 2:
                    exo2();
                    break;
                case 3:
                    exo3();
                    break;
                case 4:
                    exo4();
                    break;
                case 5:
                    exo5();
                    break;
                case 6:
                    exo6();
                    break;
                case 7:
                    exo7();
                    break;
                case 8:
                    exo8();
                    break;
            }
        }   
    }
    
    return 0;
    
}
